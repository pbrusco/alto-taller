# Pablo Brusco
#
# This file is part of Alto Taller.
#
# Alto Taller is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Alto Taller is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alto Taller.  If not, see <http://www.gnu.org/licenses/>.

if Rails.env == 'development' || Rails.env == 'test'
  def $stdout.puts_with_color(*args)
    print "\033[1;32m"
    puts_without_color *args
    print "\033[0m"
  end

  klass = class << $stdout; self; end
  klass.alias_method_chain :puts, :color

  module Kernel
    def p_with_color(*args)
      print "\033[1;32m"
      p_without_color *args
      print "\033[0m"
    end
    alias_method_chain :p, :color
  end
end
