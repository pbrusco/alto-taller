# Pablo Brusco
#
# This file is part of Alto Taller.
#
# Alto Taller is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Alto Taller is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alto Taller.  If not, see <http://www.gnu.org/licenses/>.

AltoTaller::Application.routes.draw do
  devise_for :users

  resources :problem_groups do
    resources :problems
  end

  match '/:problem_group_id' => 'students#index', as: 'students'
  match '/:problem_group_id/join' => 'students#join'
  match '/:problem_group_id/submit_info' => 'students#submit_info'
  match '/:problem_group_id/submit_state' => 'students#submit_state'
  match '/:problem_group_id/submit_teacher_state' => 'problems#submit_teacher_state'

  root :to => 'problem_groups#index'
end
