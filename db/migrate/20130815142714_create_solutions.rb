class CreateSolutions < ActiveRecord::Migration
  def change
    create_table :solutions do |t|
      t.integer :student_id
      t.integer :problem_id
      t.string :status, default: :not_started
      t.text :code

      t.timestamps
    end
  end
end
