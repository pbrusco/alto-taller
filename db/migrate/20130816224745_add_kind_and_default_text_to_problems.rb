class AddKindAndDefaultTextToProblems < ActiveRecord::Migration
  def change
    add_column :problems, :kind, :string
    add_column :problems, :default, :text
  end
end
