class CreateProblemGroups < ActiveRecord::Migration
  def change
    create_table :problem_groups do |t|
      t.integer :user_id
      t.string :title

      t.timestamps
    end
  end
end
