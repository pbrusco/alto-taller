class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.integer :problem_group_id
      t.string :guid
      t.string :info

      t.timestamps
    end
  end
end
