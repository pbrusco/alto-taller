class CreateProblems < ActiveRecord::Migration
  def change
    create_table :problems do |t|
      t.integer :problem_group_id
      t.string :title
      t.text :description
      t.text :state
      t.boolean :visible

      t.timestamps
    end
  end
end
