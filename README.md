#Alto Taller#

Proyecto creado para agilizar el dictado de talleres de computación con gran cantidad de alumnos.

La idea es lograr compartir código a los docentes y compañeros durante una clase permitiendo analizarlo entre todos de una manera sencilla.
### Características
* Creación y manejo de ejercicios por clase
* Distintos estados de progreso que los alumnos ingresan junto al código que desean compartir
* URLs para los alumnos sin necesidad de loguearse
* Sincronización automática sin necesidad de estar guardando cambios
* y más.. ver http://alto-taller.herokuapp.com

### Licencia ###
GNU GPL V3

### Ejemplo ###
![alto-taller.png](https://bitbucket.org/repo/Arn8rB/images/2325927287-alto-taller.png)