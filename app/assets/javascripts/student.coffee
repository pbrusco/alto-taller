@initStudent = (problemGroupId) ->
  translations = {
    in_progress: '<span class="label label-info">En progreso</span>',
    not_started: '<span class="label"> No arrancamos</span>',
    on_trouble: '<span class="label label-warning"> Estamos en problemas</span>',
    done: '<span class="label label-success"> Listo</span>',
  }

  class Student
    constructor: (problemGroupId) ->
      @faye = new Faye.Client(window.fayeUrl)
      @faye.disable('websocket');

      @problemGroupId = problemGroupId
      @problems = ko.observableArray();
      @state = ko.observable(state: 'initial')
      @selectedProblem = ko.observable()
      @studentInfo = ko.observable()
      @canSubmitInfo = ko.computed => $.trim(@studentInfo()).length > 0

      $.get "/#{@problemGroupId}/join", (state) =>
        @guid = state.guid
        @processState(state)

        @faye.subscribe "/problem_group/#{@problemGroupId}/student/#{@guid}", (newState) =>
          @processState(newState)

        window.setInterval(@sendStateToServer, 5000)

    processState: (state) =>
      if state.problems?
        for stateProblem in state.problems
          problem = _.find(@problems(), (p) -> p.id == stateProblem.id)
          if problem?
            problem.update(stateProblem)
          else
            @problems.push new Problem(stateProblem)

      @state(state)

    submitInfo: =>
      $.post "/#{@problemGroupId}/submit_info", {info: @studentInfo()}

    selectProblem: (problem) =>
      @selectedProblem(problem)

    sendStateToServer: =>
      return if @state().state != 'solving'

      $.post "/#{@problemGroupId}/submit_state", JSON.stringify(@forJson())

    forJson: =>
      guid: @state().guid
      state: @state().state
      problems: _.map(@problems(), (p) -> p.forJson())

  class Problem
    constructor: (data) ->
      @id = data.id
      @title = data.title
      @tip = ko.observable()
      @description = data.description
      @code = data.code
      @status = ko.observable()
      @update(data)

    update: (data) =>
      @tip(data.default)
      @status(data.status)

    showStatus: =>
      "#{translations[@status()]}"

    forJson: =>
      id: @id
      code: @code
      status: @status()

  window.model = new Student(problemGroupId)
  ko.applyBindings window.model
