@initTeacher = (problemGroupId, initialStatus) ->
  translation_classes = {
    in_progress: "label label-info"
    not_started: "label"
    on_trouble: "label label-warning",
    done: "label label-success"
  }

  translations = {
    in_progress: "En progreso",
    not_started: "No arrancaron",
    on_trouble: "En problemas",
    done: "Listo",
  }

  class Teacher
    constructor: (problemGroupId, initialStatus) ->
      @faye = new Faye.Client(window.fayeUrl)
      @faye.disable('websocket');

      @problemGroupId = problemGroupId
      @state = ko.observable()
      @problems = ko.observableArray()
      @selectedProblem = ko.observable()
      @selectedSolution = ko.observable()

      @processState(initialStatus)
      @faye.subscribe "/problem_group/#{@problemGroupId}/teacher", (newState) =>
        @processState(newState)

    processState: (state) =>
      for stateProblem in state.problems
        problem = _.find(@problems(), (p) -> p.id == stateProblem.id)
        if problem?
          problem.update(stateProblem)
        else
          @problems.push new Problem(stateProblem)

      @state(state)

    sendStateToServer: =>
      $.post "/#{@problemGroupId}/submit_teacher_state", JSON.stringify(@forJson())

    forJson: =>
      problems: _.map(@problems(), (p) -> p.forJson())

    selectProblem: (problem) =>
      @selectedProblem(problem)

    deleteProblem: (problem) =>
      $.ajax
        url: "/problem_groups/#{@problemGroupId}/problems/#{problem.id}"
        type: "DELETE"
        success: (result) ->
          @problems.remove problem

    selectSolution: (solution) =>
      @selectedSolution(solution)

  class Problem
    constructor: (data) ->
      @id = data.id
      @title = ko.observable()
      @default = ko.observable()
      @solutions = ko.observableArray()
      @visible = ko.observable()

      @update(data)

      @visible.subscribe ->
        window.model.sendStateToServer()

    update: (data) =>
      @title(data.title)
      @default(data.default)
      @updateSolutions(data)
      @visible(data.visible)

    updateSolutions: (data) =>
      for stateSolution in data.solutions
        solution = _.find(@solutions(), (s) -> s.studentId == stateSolution.student_id)
        if solution?
          solution.update(stateSolution)
        else
          @solutions.push new Solution(stateSolution)

    forJson: =>
      id: @id
      visible: @visible()

    showStatus: =>
      return "Not Visible" unless @visible()

      counts = {}

      for solution in @solutions()
        counts[solution.status()] ?= 0
        counts[solution.status()] += 1

      result = ""
      for key,count of counts
        for num in [1..count]
          result += "<span class=\"square #{translation_classes[key]}\">&nbsp;</span> "

      result


  class Solution
    constructor: (data) ->
      @studentId = data.student_id
      @status = ko.observable()
      @code = ko.observable()
      @studentInfo = ko.observable()
      @update(data)

    update: (data)=>
      @status(data.status)
      @code(data.code)
      @studentInfo(data.student_info)

    showSolutionStatus: =>
      "<span class=\"#{translation_classes[@status()]}\"> #{translations[@status()]} </span> "

  window.model = new Teacher(problemGroupId, initialStatus)
  ko.applyBindings window.model
