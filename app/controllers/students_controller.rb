# Pablo Brusco
#
# This file is part of Alto Taller.
#
# Alto Taller is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Alto Taller is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alto Taller.  If not, see <http://www.gnu.org/licenses/>.

class StudentsController < ApplicationController
  def index
    @problem_group = ProblemGroup.find params[:problem_group_id]
  end

  def join
    guid = session[:session_id]

    @problem_group = ProblemGroup.find params[:problem_group_id]
    student = @problem_group.join_student guid
    render json: @problem_group.student_status(student)
  end

  def submit_info
    guid = session[:session_id]

    @problem_group = ProblemGroup.find params[:problem_group_id]
    @problem_group.submit_student_info guid, params[:info]

    head :ok
  end

  def submit_state
    state = JSON.parse(request.raw_post)
    guid = session[:session_id]

    @problem_group = ProblemGroup.find params[:problem_group_id]
    @problem_group.process_student_state guid, state

    head :ok
  end
end
