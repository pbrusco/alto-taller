# Pablo Brusco
#
# This file is part of Alto Taller.
#
# Alto Taller is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Alto Taller is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alto Taller.  If not, see <http://www.gnu.org/licenses/>.

class ProblemsController < ApplicationController
  before_filter :load_problem_group
  before_filter :authenticate_user!

  def new
    @problem = @problem_group.problems.new
  end

  def create
    @problem = @problem_group.problems.new params[:problem]
    if @problem.save
      redirect_to problem_group_path(@problem_group)
    else
      render :new
    end
  end

  def destroy
    @problem = @problem_group.problems.find params[:id]
    @problem.destroy
    redirect_to problem_group_path(@problem_group)
  end

  def submit_teacher_state
    state = JSON.parse(request.raw_post)

    @problem_group = ProblemGroup.find params[:problem_group_id]
    @problem_group.process_teacher_state state

    head :ok
  end

  private

  def load_problem_group
    @problem_group = current_user.problem_groups.find params[:problem_group_id]
  end
end
