# Pablo Brusco
#
# This file is part of Alto Taller.
#
# Alto Taller is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Alto Taller is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alto Taller.  If not, see <http://www.gnu.org/licenses/>.

class ProblemGroupsController < ApplicationController
  before_filter :authenticate_user!

  def index
    @problem_groups = current_user.problem_groups.all
  end

  def new
    @problem_group = current_user.problem_groups.new
  end

  def show
    @problem_group = current_user.problem_groups.find params[:id]
  end

  def create
    @problem_group = current_user.problem_groups.new params[:problem_group]
    if @problem_group.save
      redirect_to problem_groups_path
    else
      render :new
    end
  end

  def destroy
    @problem_group = current_user.problem_groups.find params[:id]
    @problem_group.destroy
    redirect_to problem_groups_path
  end
end
