# Pablo Brusco
#
# This file is part of Alto Taller.
#
# Alto Taller is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Alto Taller is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alto Taller.  If not, see <http://www.gnu.org/licenses/>.

module KnockoutHelper
  def ko_link_to(text, click, options = {})
    link_to text, 'javascript:void()', options.merge(ko click: click)
  end

  def ko_text(value)
    "<!-- ko text: #{value} --><!-- /ko -->".html_safe
  end

  def ko_state_text(value)
    ko_text "state().#{value}"
  end

  def ko(hash = {})
    {'data-bind' => kov(hash)}
  end

  def kov(hash = {})
    hash.map do |k, v|
      k = "'#{k}'" if k =~ /\-/
      if v.respond_to? :to_hash
        "#{k}:{#{kov(v)}}"
      elsif k.to_s == 'valueUpdate'
        "#{k}:'#{v}'"
      elsif k.to_s == 'class'
        "'#{k}':#{v}"
      else
        "#{k}:#{v}"
      end
    end.join(',')
  end
end
