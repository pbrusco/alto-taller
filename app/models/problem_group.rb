# Pablo Brusco
#
# This file is part of Alto Taller.
#
# Alto Taller is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Alto Taller is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alto Taller.  If not, see <http://www.gnu.org/licenses/>.

class ProblemGroup < ActiveRecord::Base
  attr_accessible :title, :user_id

  validates_presence_of :title

  belongs_to :user
  has_many :problems, dependent: :destroy
  has_many :students, dependent: :destroy

  def join_student(guid)
    students.find_or_create_by_guid(guid)
  end

  def submit_student_info(guid, info)
    return if info.blank?

    student = find_student_by_guid guid
    student.info = info
    student.save!

    send_status_to_student student
  end

  def process_student_state(guid, status)
    student = find_student_by_guid(guid)
    status["problems"].each do |problem|
      solution = solution_for(problem["id"], student.id)
      solution.status = problem["status"]
      solution.code = problem["code"]
      solution.save!
    end

    send_status_to_teacher
  end

  def process_teacher_state(status)
    status["problems"].each do |problem_data|
      problem = Problem.find(problem_data["id"])
      problem.visible = problem_data["visible"]
      problem.save!
    end

    send_status_to_all_students
  end

  def student_status(student)
    student = find_student(student) unless student.is_a?(Student)

    status = student_status_without_guid(student)
    status[:guid] = student.guid
    status
  end

  def teacher_status
    data = []
    problems.each do |problem|
      data << {
        solutions: (problem.visible ? problem_data(problem) : []),
        title: problem.title,
        description: problem.description,
        id: problem.id,
        visible: problem.visible,
      }
    end
    {problems: data}
  end

  def problem_data(problem)
    data = []
    students.each do |student|
      solution = solution_for(problem.id, student.id)
      data << {
        code: solution.code,
        status: solution.status,
        student_id: student.id,
        student_info: student.info,
      }
    end
    data
  end

  def student_status_without_guid(student)
    if student.info.blank?
      return {state: :fill_info}
    end

    {
      state: :solving,
      problems: problems_status_for_student(student),
    }
  end

  def problems_status_for_student(student)
    Problem.visible.where(problem_group_id: student.problem_group_id).map do |problem|
      solution = solution_for(problem.id, student.id)
      {
        title: problem.title,
        description: problem.description,
        status: solution.status,
        code: solution.code,
        id: problem.id,
        default: problem.default,
      }
    end
  end

  def solution_for(problem_id, student_id)
    Solution.find_or_create_by_student_id_and_problem_id(student_id, problem_id)
  end

  def find_student(student_id)
    students.find(student_id)
  end

  def find_student_by_guid(guid)
    students.find_by_guid(guid)
  end

  def send_status_to_all_students
    students.each do |student|
      send_status_to_student student
    end
  end

  def send_status_to_student(student)
    student = find_student(student) unless student.is_a?(Student)

    send_to_student student.guid, student_status(student)
  end

  def send_to_student(guid, data)
    Pusher.send_to_student id, guid, data
  end

  def send_status_to_teacher
    Pusher.send_to_teacher id, teacher_status
  end

  def to_s
    title
  end
end
