# Pablo Brusco
#
# This file is part of Alto Taller.
#
# Alto Taller is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Alto Taller is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alto Taller.  If not, see <http://www.gnu.org/licenses/>.

class Problem < ActiveRecord::Base
  attr_accessible :description, :title, :user_id, :visible, :default, :kind

  belongs_to :problem_group
  has_many :solutions, dependent: :destroy

  validates_presence_of :problem_group
  validates_presence_of :title

  after_save :send_status_to_student
  after_destroy :send_status_to_student

  def send_status_to_student
    problem_group.send_status_to_all_students
  end

  def self.visible
    where(visible: true)
  end

  def to_s
    title
  end
end
