# Pablo Brusco
#
# This file is part of Alto Taller.
#
# Alto Taller is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Alto Taller is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Alto Taller.  If not, see <http://www.gnu.org/licenses/>.

class Pusher
  FAYE_URI = URI.parse FAYE_URL

  class << self
    def send_to_student(problem_group_id, student_id, data)
      send_to_channel "/problem_group/#{problem_group_id}/student/#{student_id}", data
    end

    def send_to_teacher(problem_group_id, data)
      send_to_channel "/problem_group/#{problem_group_id}/teacher", data
    end

    def send_to_channel(channel, data)
      faye_message = {channel: channel, data: data, ext: {auth_token: FAYE_TOKEN}}

      Rails.logger.debug "Faye: #{channel} -> #{data}" if Rails.logger.level >= Logger::DEBUG

      Net::HTTP.post_form(FAYE_URI, :message => faye_message.to_json)
    end
  end
end
